<?php
session_start();
require 'config.php';
require 'classes/usuarios.class.php';
// require 'classes/documentos.class.php';

if(!isset($_SESSION['logado'])) {
  header("Location: login.php");
  exit;
}

$usuarios = new Usuarios($pdo);
$usuarios->setUsuario($_SESSION['logado']);

if($usuarios->temPermissao("SECRET") == false ) {
    header("Location: index.php");
    exit;
}

?>

<h2>Página Secreta</h2></br></br>


<a href="index.php">Back</a>

