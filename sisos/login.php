<?php
session_start();
require 'config.php';
require 'classes/usuarios.class.php';

if(isset($_POST['nome'])) {
  $nome = addslashes($_POST['nome']);
  $senha = md5($_POST['senha']);

  $usuarios = new Usuarios($pdo);

  if($usuarios->fazerLogin($nome, $senha)) {
    header("Location: index.php");
    exit;
  } else {
    echo "Usuário e/ou senha estão errados !";
  }


}







/* FUNCIONANDO ANTIGO LOGIN
session_start();
require 'config.php';
require 'classes/usuarios.class.php';
*/


?>

<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>Sistema de Ordem de Serviço</title>
  <meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
  <meta name="description" content="TemplateMonster - A Responsive HTML5 Admin UI Framework">
  <meta name="author" content="TemplateMonster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/sisos_ico.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
</head>

<body class="external-page sb-l-c sb-r-c">

  <!-- Start: Main -->
  <div id="main" class="animated fadeIn" >

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper"  >

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"  ></canvas>
      </div>

      <!-- Begin: Content -->
      <section id="content" >

        <div class="admin-form theme-info" id="login1" style="width: 600px;" >

          <div class="row mb15 table-layout">

           <!-- <div class="col-xs-6 va-m pln">
              <a href="" title="Return to Dashboard">
                <img src="assets/img/logos/logo_white.png" title="TemplateMonster Logo" class="img-responsive w250">
              </a>
            </div> -->

           

          </div>

          <div class="panel panel-info mt10 br-n" >

            <div class="panel-heading heading-border bg-white">
              <span class="panel-title hidden">
                <i class="fa fa-sign-in"></i>Register</span>
              
            </div>

            <!-- end .form-header section -->
            <form method="POST">
              <div class="panel-body bg-light p30">
                <div class="row">
                  <div class="col-sm-12 pr30">

                    
                    <!--  Sessão Usuário -->
                     <div class="section">
                      <label for="username" class="field-label text-muted fs18 mb10">Usuário</label>
                      <label for="username" class="field prepend-icon">
                        <input type="text" name="nome" id="nome" class="gui-input" placeholder="Enter username">
                        <label for="username" class="field-icon">
                          <i class="fa fa-user"></i>
                        </label>
                      </label>
                    </div>
                    <!-- end section -->

                    <!--  Sessão Senha -->
                     <div class="section">
                      <label for="username" class="field-label text-muted fs18 mb10">Senha</label>
                      <label for="password" class="field prepend-icon">
                        <input type="password" name="senha" id="senha" class="gui-input" placeholder="Enter password">
                        <label for="password" class="field-icon">
                          <i class="fa fa-lock"></i>
                        </label>
                      </label>
                    </div>
                    <!-- end section -->

                  </div>
                  
                </div>
              </div>
              <!-- end .form-body section -->
              <div class="panel-footer clearfix p10 ph15">
                            
                  <input type="submit" value="Entrar" class="btn-primary btn-lg mr10 pull-left" />
                
              </div>
              <!-- end .form-footer section -->
            </form>
          </div>
        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- CanvasBG Plugin(creates mousehover effect) -->
  <script src="vendor/plugins/canvasbg/canvasbg.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    Demo.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>

<!--  LOGIN ABAIXO FUNCIONA  -->
<!-- <form method="POST">
    Usuário:</br>
    <input type="text" name="nome" /></br></br>

    Usuário:</br>
    <input type="password" name="senha" /></br></br>

    <input type="submit" value="Entrar" />
</form> -->
