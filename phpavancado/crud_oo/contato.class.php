<?php

class Contato {

    private $pdo;

    public function __construct() {
        
        $this->pdo = new PDO("mysql:dbname=crudoo;host=localhost", "root", "");
    }

    public function adicionar($email, $nome = '') {
        if($this->existeEmail($email) == false ) {
            $sql = "INSERT INTO contatos (nome, email) VALUES (:nome, :email)";
            $sql = $this->pdo->prepare($sql);
            $sql->bindValue(':nome', $nome);
            $sql->bindValue(':email', $email);
            $sql->execute();

            echo ("<strong>Usuário cadastrado com sucesso !</strong></br></br>");
            // return true;
        } else {
            echo "<strong>E-mail já existe no Banco de Dados.</strong></br></br>"; // return false;
        }
    }

    public function getNome($email) {
        $sql = "SELECT nome FROM contatos WHERE email = :email";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(':email', $email);
        $sql->execute();

        if($sql->rowCount() > 0 ) {
            $info = $sql->fetch();

            return $info['nome'];
        } else {
            return '';
        }
    }

    public function getAll() {
        $sql = "SELECT * FROM  contatos";
        $sql = $this->pdo->query($sql);

        if($sql->rowCount() >0 ) {
            return $sql->fetchAll();
        } else {
            return array();
        }
    }

    public function editar($nome, $email) {
        if($this->existeEmail($email) == true ) {
            $sql = "UPDATE contatos SET nome = :nome WHERE email = :email";
            $sql = $this->pdo->prepare($sql);
            $sql->bindValue(':nome', $nome);
            $sql->bindValue(':email', $email);
            $sql->execute();

            return true;

        } else {
            return false;
        }
    }

    public function excluir($email) {
        if($this->existeEmail($email)) {

            $sql = "DELETE FROM contatos WHERE email = :email";
            $sql = $this->pdo->prepare($sql);
            $sql->bindValue(':email', $email);
            $sql->execute();

            echo ("Usuário cadastrado com sucesso !</br></br>");

        } else {
            echo ("Não existe usuário com esse nome...</br></br>");
        }
    }

    private function existeEmail($email) {
        $sql = "SELECT * FROM contatos WHERE email = :email";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(':email', $email);
        $sql->execute();

        if($sql->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    

}




?>