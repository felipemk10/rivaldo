<?php

class Post {

    private $titulo;
    private $data;
    private $corpo;
    private $comentarios;

    public function getTitulo() {
        return $this->titulo;
    }
    public function setTitulo($t) {
        $this->titulo = $t;
    }
}

$post = new Post(); // INSTANCIANDO A CLASS Post.

$post->setTitulo ("Título de Cartório."); // ATRIBUINDO VALOR A UMA PROIEDADE ($titulo) DA CLASS Post.

echo $post->getTitulo();  // APRESENTANDO O CONTEÚDO DE UMA PROPIEDADE DA CLASS Post.


// var_dump($post); VERIFICANDO O CONTEÚDO DA CLASS POST QUE TAMBÉM É RECONHECIDO COMO OBJETO E SUAS PROPIEDADES.


// echo "<strong> Qual origem desse título ? </strong> </br></br> ". $post->getTitulo(); 




?>