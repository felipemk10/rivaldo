<?php
session_start();
require 'config.php';
require 'funcoes.php';

// Criando uma sessão

if(empty($_SESSION['login'])) {
    header("Location: login.php");
    exit;
}

$id = $_SESSION['login'];

$sql = $pdo->prepare("SELECT
usuarios.nome,
patentes.nome as p_nome
FROM usuarios
LEFT JOIN patentes ON patentes.id = usuarios.patente
WHERE usuarios.id = :id");
$sql->bindValue(":id", $id);    
$sql->execute();

if($sql->rowCount() >0 ) {
    $sql = $sql->fetch();
    $nome = $sql['nome'];
    $p_nome = $sql['p_nome'];
} else {
    header("Location: login.php");
    exit;
}

// INICIO DO ARRAY

$lista = listar($id, $limite);



// echo $_SESSION['nomecompleto'];
?>


<p align="right">Usuário Logado: <b><?php echo $_SESSION['nomecompleto']. ' [<span style="color: #3CB371">&nbsp;'.$p_nome.'&nbsp</span>] '; ?></b>
<a href="sair.php" style="color: red">|&nbsp;<strong>Sair</strong></a></p>
<hr>


<h2 align="center">Sistema Marketing MN</h2><hr>

<a href="cadastrar.php" >Cadastrar Novo Usuário</a><hr></br></br>

<h2 align="center">Usuários Associados</h2><hr></br></br>

<?php exibir($lista); ?>
